import type { Snippet } from 'svelte';

export type EventHandler<Event> = (event: Event) => void;
export type Props<T = unknown> = T & { children: Snippet; style?: string };
