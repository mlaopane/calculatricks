# Calculatrice micro-entreprise

Permet un calcul théorique des revenus net pour une micro-entrerise.  
La calculatrice calcule les cotisations ainsi que les impôts sur le revenu.

![Screenshot of the Single Page Application](./screenshot.png 'Screenshot of the Single Page Application')

## Contribute

Make sure you are using the correct `pnpm` and `node` versions defined in the [package.json](./package.json)

### Install the dependencies

```bash
pnpm install
```

### Start the dev server

```bash
pnpm run dev
```

### Run the tests

```bash
pnpm test
```

### Build the web app

```bash
pnpm run build
```
